from django.db import models


class Product(models.Model):
    name = models.CharField(max_length=150, blank=False, null=False, unique=False)
    description = models.CharField(max_length=250, blank=False, null=False, unique=False)
