from django.db import models


class Order(models.Model):
    delivery_date_time = models.DateTimeField(blank=True, null=True)
    finalized = models.BooleanField(default=False)
