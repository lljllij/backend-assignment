from django.db import models

from webstore_api.models import Order
from webstore_api.models.product import Product


class OrderItem(models.Model):
    product = models.ForeignKey(Product, verbose_name="Product", on_delete=models.DO_NOTHING)
    quantity = models.IntegerField(default=1, blank=False)
    order = models.ForeignKey(Order, verbose_name="Order", on_delete=models.CASCADE)
