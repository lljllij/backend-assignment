from rest_framework import viewsets
from webstore_api.models import Order
from webstore_api.serializers.order import OrderSerializer


class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
