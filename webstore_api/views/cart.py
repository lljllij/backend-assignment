from rest_framework import status, views
from rest_framework.response import Response
from webstore_api.models import OrderItem, Order, Product
from webstore_api.serializers.order import DatetimeSerializer, OrderSerializer
from rest_framework.decorators import api_view

from webstore_api.serializers.order_item import OrderItemSerializer, IdSerializer


class CartView(views.APIView):
    @api_view(["GET"])
    def current_items(self):
        order = Order.objects.get_or_create(finalized=False)
        items = OrderItem.objects.filter(order=order[0]).all()
        data = {}
        if items:
            serializer = OrderItemSerializer(items, many=True)
            data = serializer.data
        return Response({'items': data})

    @api_view(["POST"])
    def add_item(self):
        serializer = IdSerializer(data=self.data)
        if not serializer.is_valid():
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data={"error": "Invalid request data."})
        item_id = serializer.data["item_id"]
        order = Order.objects.get_or_create(finalized=False)
        item_to_add = Product.objects.filter(pk=item_id)
        if not item_to_add.exists():
            return Response(status=status.HTTP_404_NOT_FOUND,
                            data={"error": "Item doesn't exist."})

        item_added = OrderItem.objects.get_or_create(product_id=item_id, order=order[0])
        if not item_added[1]:
            item_added[0].quantity = item_added[0].quantity + 1
            item_added[0].save()
        serializer = OrderItemSerializer(item_added[0])
        return Response(status=status.HTTP_200_OK, data={'item': serializer.data})

    @api_view(["POST"])
    def remove_item(self):
        serializer = IdSerializer(data=self.data)
        if not serializer.is_valid():
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data={"error": "Invalid request data."})
        item_id = serializer.data["item_id"]
        order = Order.objects.filter(finalized=False)
        if not order.exists():
            return Response(status=status.HTTP_404_NOT_FOUND,
                            data={"error": "No cart created. Add an item to create a cart."})
        item_to_delete = OrderItem.objects.filter(product_id=item_id, order=order[0])
        if not item_to_delete.exists():
            return Response(status=status.HTTP_404_NOT_FOUND,
                            data={"error": "No such item in cart."})
        item_to_delete[0].delete()
        return Response(status=status.HTTP_200_OK)

    @api_view(["PATCH"])
    def delivery_date(self):
        request_serializer = DatetimeSerializer(data=self.data)
        if not request_serializer.is_valid():
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data={"error": "Invalid request data."})
        delivery_date_time = request_serializer.data["delivery_date_time"]
        order = Order.objects.filter(finalized=False)
        if not order.exists():
            return Response(status=status.HTTP_404_NOT_FOUND,
                            data={"error": "No cart created. Add an item to create a cart."})
        order[0].delivery_date_time = delivery_date_time
        order[0].save()
        order_serializer = OrderSerializer(order[0])
        return Response(status=status.HTTP_200_OK, data={'item': order_serializer.data})

    @api_view(["PATCH"])
    def finalize(self):
        order = Order.objects.filter(finalized=False)
        if not order.exists():
            return Response(status=status.HTTP_404_NOT_FOUND,
                            data={"error": "No cart created. Add an item to create a cart."})
        if not order[0].delivery_date_time:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data={"error": "Please set a delivery date first."})
        order[0].finalized = True
        order[0].save()
        order_serializer = OrderSerializer(order[0])
        return Response(status=status.HTTP_200_OK, data={'item': order_serializer.data})
