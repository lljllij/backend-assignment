from rest_framework import viewsets
from webstore_api.models import Product
from webstore_api.serializers.product import ProductSerializer


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    # def get_queryset(self):
    #     return().get_queryset()

