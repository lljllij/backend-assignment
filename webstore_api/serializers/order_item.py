from rest_framework import serializers
from webstore_api.models import OrderItem


class OrderItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItem
        fields = '__all__'


class IdSerializer(serializers.Serializer):
    item_id = serializers.IntegerField(required=True)
