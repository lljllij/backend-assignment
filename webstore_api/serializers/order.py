from rest_framework import serializers
from webstore_api.models import Order


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = '__all__'


class DatetimeSerializer(serializers.Serializer):
    delivery_date_time = serializers.DateTimeField(required=True)
