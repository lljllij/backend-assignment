"""autocompany URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

# from .views.cart import Finalize, DeliveryDate, RemoveItem, AddItem, CurrentItems
from .views.product import ProductViewSet
from .views.order import OrderViewSet
from .views.cart import CartView

urlpatterns = [
    path('cart/items/',
         CartView.current_items, name='items_in_cart'),
    path('cart/add/',
         CartView.add_item, name='add_item'),
    path('cart/remove/',
         CartView.remove_item, name='remove_item'),
    path('cart/set-delivery-date/',
         CartView.delivery_date, name='remove_item'),
    path('cart/finalize/',
         CartView.finalize, name='remove_item'),

    path('product/', ProductViewSet.as_view({"get": "list", "post": "create"}), name="products"),
    path('product/<int:pk>', ProductViewSet.as_view({"get": "retrieve"}), name="product"),
]
