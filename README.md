# Build container and start app
`docker-compose up -d --build`

# See all available products in the shop:
`http://127.0.0.1:8000/rest-api/product/`

# Current items in shopping cart:
`http://127.0.0.1:8000/rest-api/cart/items/`

# Please check the postman collection for all the api endpoints
`Alecia.postman_collection.json`

